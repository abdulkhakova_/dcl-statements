GRANT CONNECT ON DATABASE dvdrental TO rentaluser;

GRANT SELECT ON TABLE customer TO rentaluser;

-- Assuming the "customer" table is in the "public" schema
-- Grant USAGE on the schema to rentaluser
GRANT USAGE ON SCHEMA public TO rentaluser;

-- Grant SELECT permission on the "customer" table to rentaluser
GRANT SELECT ON TABLE public.customer TO rentaluser;

-- Now, rentaluser should be able to execute the SELECT query on the customer table
SELECT * FROM customer;

   CREATE GROUP rental;
   GRANT rental TO rentaluser;


-- Grant INSERT and UPDATE permissions on the rental table to the rental group
GRANT INSERT, UPDATE ON TABLE rental TO rental;

   -- Execute Insert and Update statements as a member of the "rental" group

   REVOKE INSERT ON rental FROM rental;


-- Revoke the INSERT permission on the rental table from the rental group
REVOKE INSERT ON TABLE rental FROM rental;

-- Assuming customer_id is the primary key in the customer table
CREATE ROLE client_John_Doe;

-- Grant USAGE on the schema to the client role
GRANT USAGE ON SCHEMA public TO client_John_Doe;

-- Grant SELECT permission on the rental and payment tables to the client role
GRANT SELECT ON TABLE rental, payment TO client_John_Doe;

-- Additional constraints based on customer's rental and payment history
DO $$ 
DECLARE
   user_role_name TEXT;
BEGIN
   SELECT 'client_'  first_name  '_'  last_name INTO user_role_name
   FROM customer
   WHERE payment_history IS NOT NULL AND rental_history IS NOT NULL
   LIMIT 1;
   
   IF user_role_name IS NOT NULL THEN
      EXECUTE 'GRANT '  user_role_name  ' TO '  quote_ident(user_role_name);
   END IF;
END $$;

-- Assuming customer_id is the primary key and 'John' and 'Doe' are the customer's first and last names
SET ROLE client_John_Doe;

-- Now, the user with the client role can only access their own data
SELECT * FROM rental;
SELECT * FROM payment;

-- Assuming customer_id is the primary key and 'John' is the customer's first name
SET ROLE client_John;

-- Now, the user with the client role can only access their own data
SELECT * FROM rental WHERE customer_id = <John's Customer ID>;
SELECT * FROM payment WHERE customer_id = <John's Customer ID>;